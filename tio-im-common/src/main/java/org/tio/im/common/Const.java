package org.tio.im.common;

/**
 * 
 * @author tanyaowu 
 *
 */
public interface Const
{
	String authkey = "mobo";
	
	int SERVER_PORT = 8888;
	
	public static final String CHARSET = "utf-8";
	
	public static final String TO = "to";
	
	public static final String CHANNEL = "channel";
	
	public static final String PACKET = "packet";
	
	public static final String STATUS = "status";
}
