package org.tio.im.common.http;

/**
 * 
 * @author tanyaowu 
 *
 */
public interface HttpResponseHeader
{
	String Content_Length = "Content-Length";
}
